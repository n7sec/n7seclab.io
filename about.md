---
layout: page
title: About
permalink: /about/
---

You can connect with N7 Sec on <a href="https://twitter.com/n7sec" target="blank" rel="noopener">Twitter</a>.

All of our code is open-source on <a href="https://gitlab.com/n7sec" target="blank" rel="noopener">Gitlab</a>.

# Credits

* Site hosted using <a href="https://cloudflare.com" target="blank" rel="noopener">Cloudflare</a> and <a href="https://gitlab.com" target="blank" rel="noopener">Gitlab</a> pages
* Site built using <a href="https://github.com/jekyll/jekyll" target="blank" rel="noopener">jekyll</a>
* Site uses a customized version of the <a href="https://github.com/jekyll/minima" target="blank" rel="noopener">minima jekyll theme</a>
* Article table of contents are built using <a href="https://github.com/allejo/jekyll-toc" target="blank" rel="noopener">jekyll-toc</a>
* Mass Effect icons by <a href="http://www.rw-designer.com/user/5920" target="blank" rel="noopener">Sirea</a>

<!--
<a href="" target="blank" rel="noopener"></a>
![n7sec](/images/icons/N7.ico){:class="img-responsive"}
-->
