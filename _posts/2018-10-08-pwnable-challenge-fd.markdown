---
layout: post
title:  "Pwnable.kr Challenge: fd"
date:   2018-10-08 17:13:42 -0400
categories: pwnable.kr
---

The `fd` challenge can be found at: <a href="http://pwnable.kr/play.php" target="blank" rel="noopener">http://pwnable.kr/play.php</a>.

This post describes one approach for solving this challenge. You are encouraged to try the challenge yourself before reading this write up.

## Challenge
```
Mommy! what is a file descriptor in Linux?

ssh fd@pwnable.kr -p2222 (pw:guest)
```

If you SSH into the given host, you'll be greeted by the Pwnable.kr logo annnnd...not much else. Hmm...where do we go from here?

## Poking About
Probably best to check out the directory they dropped us into. This being challenge #1 in the toddler's bottle, let's naively assume that
they give us pretty much everything we need, so a `-l` option should suffice:

{% highlight shell %}
fd@ubuntu:~$ ls -l
-r-sr-x--- 1 fd_pwn fd   7322 Jun 11  2014 fd
-rw-r--r-- 1 root   root  418 Jun 11  2014 fd.c
-r--r----- 1 fd_pwn root   50 Jun 11  2014 flag
{% endhighlight %}

So we have three files:
1.  A little program called `fd`
1.  Some C source code in the `fd.c` file
1.  Annnnd...the flag!!

So, the `flag` file is readable by the `fd_pwn` user and the `root` group. I wonder if we have access to either of those...

{% highlight shell %}
fd@ubuntu:~$ whoami
fd
fd@ubuntu:~$ groups
fd
{% endhighlight %}

Bummer. So we are unable to read the flag file, which we can confirm:

{% highlight shell %}
fd@ubuntu:~$ cat flag
cat: flag: Permission denied
{% endhighlight %}

:-( It's never that easy...but worth a shot. Now, one other thing to note: We can run the `fd` program, which you can confirm via:

{% highlight shell %}
fd@ubuntu:~$ ./fd
pass argv[1] a number
{% endhighlight %}

Now, why are we allowed to run this program? Looking at its permissions, you'll see that the execute permission bit is set for `fd` group, which we have just confirmed is our only group. Lucky us :)

## The fd.c Source
OK - next things next, let's have a look at the `fd.c` source code file:

{% highlight c %}
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
char buf[32];
int main(int argc, char* argv[], char* envp[]){
  if(argc<2){
    printf("pass argv[1] a number\n");
    return 0;
  }
  int fd = atoi( argv[1] ) - 0x1234;
  int len = 0;
  len = read(fd, buf, 32);
  if(!strcmp("LETMEWIN\n", buf)){
    printf("good job :)\n");
    system("/bin/cat flag");
    exit(0);
  }
  printf("learn about Linux file IO\n");
  return 0;
}
{% endhighlight %}

Hmmm...what's going on here? Well, first, how's about a little C refresher. Let's start with the definition of the `main` function since that a) it looks kind of important and b) the program told us to "pass argv[1] a number" when we ran it a minute ago:

{% highlight c %}
main(int argc, char* argv[], char* envp[])
{% endhighlight %}

In this code snippet [[1]](#ref1):
- `argc`: Argument count. Contains the number of arguments passed to the program. So if we invoke the executable this code compiles to using `$ fd foo`, `argc` equals 2 since we have one argument (foo) + the program's name (see below).
- `argv[]`: Argument vector. A one-dimensional array of strings, each of which is one of the arguments that was passed to the program.
    - The file name of the program being run is also included in the vector as the first element; the value of argc counts this element.
    - A null pointer always follows the last element: argv[argc] is this null pointer.
- `envp`: Gives the program's environment (Unix-like systems only).
    - It is the same as the value of the `environ` variable.
    - However, `envp` is not POSIX-compliant, so it's best to use 2-arg form (so just `argc` and `argv[]`) and use `environ` instead of `envp`.

OK, great. So, what happens if follow directions and "pass argv[1] a number"?

{% highlight shell %}
fd@ubuntu:~$ ./fd 42
learn about Linux file IO
{% endhighlight %}

:-( Gosh - couldn't they at least say "please"?

## Describing File Descriptors
Well, this challenge _does_ keep talking about "file descriptors" (and in fact is named `fd`, which it probably isn't too great a leap to imagine stands for "file descriptor"), so it would be good to nail down exactly what "file descriptors" are.

Our friend Google links to a very helpful Stack Overflow thread [[2]](#ref2), which I will summarize:
- When a process is using a file (reading, writing, etc.), it does so through an interface provided by the kernel - processes don't actually interact with files directly.
- File descriptors are the mechanism that the kernel provides for interacting with files. They are just numbers (i.e., 100) that the kernel knows how to correlate with open/in-use files.
- Since, in Unix-based systems, everything is a file, a file descriptor can actually reference any number of things (i.e., a USB device, a socket, etc.).
- By convention, shells in Unix Systems associate the file descriptor `0` with Standard Input for a process, file descriptor `1` with Standard Output, and file descriptor `2` with Standard Error.

## Putting it All Together
Hmmm...so let's revisit that source code with all of the above in mind:

{% highlight c %}
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
char buf[32];
int main(int argc, char* argv[], char* envp[]){
  // This program is expecting 2 arguments. We know 1 of those is
  // automatically the program's name, and that printf tells us the other
  // should be a number!
  if(argc<2){
    printf("pass argv[1] a number\n");
    return 0;
  }
  // The following line converts whatever number we pass to the program
  // into an integer and then subracts 0x1234 (a hex number equal to 4660
  // decimal) from our number. It then assigns this value to the fd
  // variable. fd...hmmm...
  int fd = atoi( argv[1] ) - 0x1234;
  int len = 0;
  // OK - now the program is reading into a buffer whatever we type into
  // the file descriptor computed above (NOTE: Don't ever do this!)
  len = read(fd, buf, 32);
  // If the contents of our buffer are equal to the string "LETMEWIN\n",
  // this little program will ask the system to give us the flag!
  if(!strcmp("LETMEWIN\n", buf)){
    printf("good job :)\n");
    system("/bin/cat flag");
    exit(0);
  }
  printf("learn about Linux file IO\n");
  return 0;
}
{% endhighlight %}

Once again, the hex number `0x1234` converts to the decimal number `4660`. Since this program is kind enough to read whatever we type into whatever file descriptor we can get it to compute, let's get it to compute file descriptor `1`, or Standard Output (aka. whatever we type into our terminal program).

To do that, we need to get `atoi( argv[1] ) - 0x1234;` to return a value of `1`. Since our command line arg is getting converted to an integer, we need to pass an int whose hex value is 1 greater than the hex number `0x1234`. Since `0x1234` hex is `4660` decimal, we need to pass our program `4661` decimal which is `0x1235` hex.

{% highlight shell %}
fd@ubuntu:~$ ./fd 4661

{% endhighlight %}

Sweet - we didn't get the message to go learn about Linux file I/O, and it actually looks like the program is waiting for some input from us, so let's give it some. Remember, whatever we type is getting compared to the string `LETMEWIN\n`, so let's type `LETMEWIN` and then hit enter which will pass the newline (`\n`) character to the program:

{% highlight shell %}
fd@ubuntu:~$ ./fd 4661
LETMEWIN
good job :)
<-- FLAG REDACTED -->
{% endhighlight %}

Yay! We were able to reason our way through this little program and figure out how to get it to give us the flag. Along the way, we reviewed some C language notation, learned about Linux I/O conventions, and discovered file descriptors.

Thanks for reading. I hope this little write up was helpful. Please <a href="https://twitter.com/n7sec" target="blank" rel="noopener">get in touch on Twitter</a> with any questions, comments, or errata.

# References

[1] <a id="ref1"><a href="https://www.gnu.org/software/libc/manual/html_node/Program-Arguments.html" target="blank" rel="noopener">https://www.gnu.org/software/libc/manual/html_node/Program-Arguments.html</a></a>

[2] <a id="ref2"><a href="https://stackoverflow.com/questions/5256599/what-are-file-descriptors-explained-in-simple-terms" target="blank" rel="noopener">https://stackoverflow.com/questions/5256599/what-are-file-descriptors-explained-in-simple-terms</a></a>
