---
layout: post
title:  "Hacker Setup: A Kali Linux C Programming and Debugging Environment"
date:   2018-10-10 10:29:55 -0400
categories: hacker-setup
---

The goal of this post is to set up all of the basic tools we need for working with C code on Kali Linux and then cover the basic flow for using each of them. In order to successfully write, debug, and decompile C programs in Kali Linux, we'll first need to lay a little groundwork.

## Groundwork
### Install Tools
First, make sure you have all of the necessary tools installed:
- **Git**: Source code version control tool
- **gcc**: The GNU compiler
- **gdb**: The GNU debugger
- **make**: Build program used by many C projects

Most if not all of these tools _should_ be pre-installed in Kali Linux. However, just to be sure, you can open a terminal prompt and enter:

{% highlight shell %}
sudo apt update && \
sudo apt -y install git && \
sudo apt -y install gcc && \
sudo apt -y install gdb && \
sudo apt -y install make
{% endhighlight %}

### Set Up Environment
Next, you'll want to enable dumping of core files (these are used to view the state of a program that crashes). The simplest approach is to add the following to either your `.bashrc` or `.bash_profile` (located in your user's home directory):

{% highlight shell %}
ulimit -c unlimited
{% endhighlight %}

You can confirm that the ulimit is properly set for your shell by entering the following into your terminal:
{% highlight shell %}
$ ulimit -c
unlimited
{% endhighlight %}

If the command prints `unlimited`, you're good to go.

## C Programming Flow
Once your tools and environment are all set up, here's how to actually use them!

### Compiling
{% highlight shell %}
# Simple compile
$ cc example.c

# Compile with:
## - Stack set to dword-size increments (prevent gcc optimizations)
## - Debugging enabled
$ cc -mpreferred-stack-boundary=2 -ggdb example.c -o example
{% endhighlight %}

### Debugging
{% highlight shell %}
# Here, example is the name of a program compiled above
# Once in gdb, you can issue it varous commands
# This deserves it own blog post...
$ gdb example
# Disassemble a function in your program
(gdb) disas function_name

# Various ways to set breakpoints
(gdb) break line_number
(gdb) break memory_address
(gdb) break [file_name]:line_number
(gdb) break [file_name]:func_name

# After setting breakpoints, run!
(gdb) run [args]

# Print out a variable's current value
(gdb) print variable_name
(gdb) p variable_name
{% endhighlight %}

### Inspecting Crashes
{% highlight shell %}
# Inspect a core dump file
$ gdb -q -c core_dump_file
{% endhighlight %}

## Further Reading
The above just gets you up and running with some basic commands under your belt. For further reading, check out:
- <a href="https://www.thegeekstuff.com/2010/03/debug-c-program-using-gdb" target="blank" rel="noopener">https://www.thegeekstuff.com/2010/03/debug-c-program-using-gdb</a>
- <a href="https://www.wiley.com/en-us/The+Shellcoder%27s+Handbook%3A+Discovering+and+Exploiting+Security+Holes%2C+2nd+Edition-p-9780470080238" target="blank" rel="noopener">The Shell Coder's Handbook</a>

Thanks for reading. I hope this little overview was helpful. Please <a href="https://twitter.com/n7sec" target="blank" rel="noopener">get in touch on Twitter</a> with any questions, comments, or errata.
